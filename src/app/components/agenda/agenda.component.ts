import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import * as Notiflix from 'notiflix';
import { Loading } from 'notiflix';

@Component({
  selector: 'app-agenda',
  templateUrl: './agenda.component.html',
  styleUrls: ['./agenda.component.css']
})
export class AgendaComponent implements OnInit {


  datos: agregarCitaI[] = [
    // {nombre: '', apellido: '', correo: '', celular: '', mensaje: '', fecha: '', hora: ''}
  ]




  formulario!: FormGroup;

  mensaje!:string;
  enviado:string = "CITA AGENDADA"

  constructor(private router :ActivatedRoute,
              private fb: FormBuilder) { 
                this.crearFormulario();

    this.router.params.subscribe(parametro=>{
      console.log(parametro);
      
    })
  }

  ngOnInit(): void {
     
  }

  get nombreNoValido() {
    return this.formulario.get('nombre')?.invalid && this.formulario.get('nombre')?.touched;
  }

  get apellidoNoValido() {
    return this.formulario.get('apellido')?.invalid && this.formulario.get('apellido')?.touched;
  }
 
  get correoNoValido() {
    return this.formulario.get('correo')?.invalid && this.formulario.get('correo')?.touched;
  }

  get celularNoValido() {
    return this.formulario.get('celular')?.invalid && this.formulario.get('celular')?.touched;
  }

  get horaNoValido() {
    return this.formulario.get('hora')?.invalid && this.formulario.get('hora')?.touched;
  }

  get fechaNoValido() {
    return this.formulario.get('fecha')?.invalid && this.formulario.get('fecha')?.touched;
  }

  get mensajeNoValido() {
    return this.formulario.get('mensaje')?.invalid && this.formulario.get('mensaje')?.touched;
  }

  crearFormulario(): void {
    this.formulario = this.fb.group({
       // Valores del array:
       // 1er valor: El valor por defecto que tendra
       // 2do valor: Son los validadores sincronos
       // 3er valor: Son los validadores asincronos
      nombre: ['', [Validators.required, Validators.minLength(4)]],
      apellido: ['', [Validators.required, Validators.minLength(4), Validators.pattern(/^[a-zA-ZñÑ\s]+$/)]],
      correo: ['', [Validators.required, Validators.pattern(/^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/)]],
      celular: ['', [Validators.required, Validators.minLength(8), Validators.pattern(/^[1-9]\d{6,10}$/)]],
      hora: ['', Validators.required],
      fecha: ['', Validators.required],
      mensaje:['', Validators.required]
      });
  }



  guardar(): void{
     Notiflix.Loading.standard('Cargando...',{
      //  svgColor: 'rgba(255, 99, 71, 0.8)',
       svgColor: 'rgba(0,239,129,0.7)',
     });
     Notiflix.Loading.change('Cita en progreso...');
    console.log(this.formulario.value);
    this.agregarCita();
     this.limpiarFormulario();
       this.mensaje = this.enviado;
        setTimeout(() => {
         this.mensaje = ""
        },2000);
         Notiflix.Loading.remove(2500);
  }

  agregarCita(){
    const usuario: agregarCitaI = {
      nombre:this.formulario.value.nombre,
      apellido:this.formulario.value.apellido,
      correo:this.formulario.value.correo,
      celular:this.formulario.value.celular,
      fecha:this.formulario.value.fecha,
      hora:this.formulario.value.hora,
      mensaje:this.formulario.value.mensaje,
    }

    console.log(usuario);
    this.agregarCitaEnTabla(usuario) 
  }


  agregarCitaEnTabla(cita:agregarCitaI){
    // this.datos.push(cita)
    console.log('empezando local');
    
    if (localStorage.getItem('reunion')===null){
      this.datos=[]
      this.datos.push(cita)
      localStorage.setItem('reunion',JSON.stringify(this.datos))
    }

    else { this.datos = JSON.parse(localStorage.getItem('reunion')||'');
    this.datos.push(cita)
    localStorage.setItem('reunion',JSON.stringify(this.datos))
  }

  console.log(this.datos);

  }
 

  limpiarFormulario(): void {
    this.formulario.reset();
  //  this.forma.reset({
  //    nombre: 'Pedro'
  //  });
 }

//  eliminarUsuario(usuario: string) {
//   this.datos = this.datos.filter(data => {
//     return data.nombre !== usuario;
//   });
// }

// buscarUsuario(id: string): agregarCitaI{
//   //o retorna un json {} vacio
//   return this.datos.find(datos => datos.nombre === id) || {} as agregarCitaI;
// }

  mostrar(){
 
  let resultado = JSON.parse(localStorage.getItem('reunion')|| '');
  console.log(resultado);
  document.getElementById("final")?.scrollIntoView({behavior:"smooth", block: "end"});

  this.datos = resultado;
  console.log(this.datos,"imprimir");
  

  }


  remove(usuario:agregarCitaI){
      const responce = confirm('Esta seguro de que quiere eliminar' )
    
      if(responce){
        for(let i = 0; i < this.datos.length; i++){
          if(usuario == this.datos[i]){
            console.log('eliminando...');
            this.datos.splice(i,1);
            localStorage.setItem('reunion',JSON.stringify(this.datos))  
            console.log("finalll");
            }
            console.log("for");
            
          }}

            // this.datos = this.datos.filter(data => {
            //   return data.nombre !== usuario.nombre;
            // });
         
            
          }



//MODO OSCURO
  alerta: string = "alerta exitosa";
  loading: boolean = false;
      
  propiedades = {
    error: false
      };
        
   ejecutar(): void{
     this.loading = true;
         setTimeout(() => {
         this.loading = false;
         }, 3000);
          }




 
}





interface agregarCitaI{
  nombre: string,
  apellido: string,
  correo: string,
  celular: string,
  mensaje: string,
  fecha: string,
  hora: string
  
}


