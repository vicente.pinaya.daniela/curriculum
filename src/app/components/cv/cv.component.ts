import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-cv',
  templateUrl: './cv.component.html',
  styleUrls: ['./cv.component.css']
})
export class CVComponent implements OnInit {


  // sidebar1:void;
  // cargarSidebaqr :void;


  constructor(private router:ActivatedRoute) {
    this.router.params.subscribe(parametros =>{
      console.log(parametros);
      
    })
   }

  ngOnInit(): void {
  }

  inicio(){
    document.getElementById("inicio")?.scrollIntoView({behavior:"smooth", block:"start"});
  }
  sobremi(){
    document.getElementById("sobremi")?.scrollIntoView({behavior:"smooth", block: "start"});
  }
  formacion(){
    document.getElementById("formacion")?.scrollIntoView({behavior:"smooth", block: "start"});
  }
  experiencia()
  {
    document.getElementById("experiencia")?.scrollIntoView({behavior:"smooth", block: "start"});
  }
  servicios()
  {
    document.getElementById("servicios")?.scrollIntoView({behavior:"smooth", block: "start"});
  }
  portafolio()
  {
    document.getElementById("portafolio")?.scrollIntoView({behavior:"smooth", block: "start"});
  }
  contacto()
  {
    document.getElementById("contacto")?.scrollIntoView({behavior:"smooth", block: "start"});
  }


  // const sidebar1 = document.getElementById('sidebar');
  // const cargarSidebaqr = (entradas, observador) =>{}
  // const observador = new IntersectionObserver(this.cargarSidebaqr,{
  //   root:null,
  //   rootMargin:'0px 0px 0px 0px'
  // })









  alerta: string = "alerta exitosa";
  loading: boolean = false;

  propiedades = {
    error: false
  };


 

  ejecutar(): void{
    this.loading = true;
    setTimeout(() => {
      this.loading = false;
    }, 3000);
  }


}


