import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {

  forma!: FormGroup;
  mensaje!:string;
  enviado:string = "mensaje enviado"

  constructor(private router :ActivatedRoute, private fb: FormBuilder) {
    this.crearFormulario();
    // this.cargarDataAlFormulario2();

    this.router.params.subscribe(parametro=>{
      console.log(parametro);
      
    })
  }

  ngOnInit(): void {
  }

 
   get nombreNoValido() {
     return this.forma.get('nombre')?.invalid && this.forma.get('nombre')?.touched;
   }

   get apellidoNoValido() {
     return this.forma.get('apellido')?.invalid && this.forma.get('apellido')?.touched;
   }
  
   get correoNoValido() {
     return this.forma.get('correo')?.invalid && this.forma.get('correo')?.touched;
   }

   get mensajeNoValido() {
     return this.forma.get('mensaje')?.invalid && this.forma.get('mensaje')?.touched;
   }


     crearFormulario(): void {
       this.forma = this.fb.group({
          // Valores del array:
          // 1er valor: El valor por defecto que tendra
          // 2do valor: Son los validadores sincronos
          // 3er valor: Son los validadores asincronos
         nombre: ['', [Validators.required, Validators.minLength(4)]],
         apellido: ['', [Validators.required, Validators.minLength(4), Validators.pattern(/^[a-zA-ZñÑ\s]+$/)]],
         correo: ['', [Validators.required, Validators.pattern(/^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/)]],
         mensaje:['', Validators.required]
         });
     }
  
      // MODIFICAR EL FORMULARIO  son los datos con los que empieza ..parecido a un placeholder pero llenado 
      // cargarDataAlFormulario2(): void{
      //  this.forma.patchValue({
      //  apellido: 'Perez'
      //    });
      //    }

     Enviar(): void{
       console.log(this.forma.value)
       this.limpiarFormulario();
       this.mensaje = this.enviado;
        setTimeout(() => {
         this.mensaje = ""
        },2000);
     }

     limpiarFormulario(): void {
        this.forma.reset();
      //  this.forma.reset({
      //    nombre: 'Pedro'
      //  });
     }

 




     alerta: string = "alerta exitosa";
     loading: boolean = false;
   
     propiedades = {
       error: false
     };
   
   
    
   
     ejecutar(): void{
       this.loading = true;
       setTimeout(() => {
         this.loading = false;
       }, 3000);
     }
 
}


