
 import { RouterModule, Routes } from "@angular/router";

 import { AgendaComponent } from "./components/agenda/agenda.component";
 import { CVComponent } from "./components/cv/cv.component";
 import { InicioComponent } from "./components/inicio/inicio.component";
import { SidebarComponent } from "./components/sidebar/sidebar.component";


 const APP_ROUTES: Routes = [
   {path: 'cv', component: CVComponent},
   {path: 'agenda', component:AgendaComponent},
   {path:'contacto', component: InicioComponent},

   {path: '**' ,pathMatch:'full', redirectTo :'cv'}
 
 
  ];

 export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);