import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';


import { AppComponent } from './app.component';
import { CVComponent } from './components/cv/cv.component';
 //import { AppRoutingModule } from './app-routing.module';

//Rutas
import { APP_ROUTING } from './app.routes';
import { InicioComponent } from './components/inicio/inicio.component';
import { NavbarComponent } from './components/extras/navbar/navbar.component';
import { AgendaComponent } from './components/agenda/agenda.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SidebarComponent } from './components/sidebar/sidebar.component';




@NgModule({
  declarations: [
    AppComponent,
    CVComponent,
    NavbarComponent,
    AgendaComponent,
    InicioComponent,
    SidebarComponent,

  
  ],
  imports: [
    BrowserModule,
    // AppRoutingModule
     APP_ROUTING,
     ReactiveFormsModule
   
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
